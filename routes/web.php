<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\AdminOrderController;
use App\Http\Controllers\AdminReviewController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CategoryController::class, 'index'])->name('main');

Route::middleware(['auth'])->group(function () {
    Route::middleware(['admin.check:admin'])->group(function () {
        Route::resource('admin_category', AdminCategoryController::class);
        Route::delete('admin_category/delete_img/{id}', [AdminCategoryController::class, 'deleteMedia'])->name('delete_img');
        Route::resource('admin_review', AdminReviewController::class);
        Route::resource('admin_order', AdminOrderController::class);
    });
});

Route::prefix('category')->group(function () {
    Route::get('/{slug}', [CategoryController::class, 'showCategory'])->name('category');
});

Route::prefix('order')->group(function () {
    Route::view('/', 'forms.order_form')->name('order_form');
    Route::post('/create', [OrderController::class, 'create'])->name('order.create');
});

Route::prefix('review')->group(function () {
    Route::view('/', 'forms.review_form')->name('review_form');
    Route::post('/create', [ReviewController::class, 'create'])->name('review.create');
});

Auth::routes();
