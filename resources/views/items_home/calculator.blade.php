
<section id="#calculator" class="page-home__calculator calculator">
    <div class="calculator__select">
        <div class="calculator__select-container">
            <h3 class="calculator__title"> расчет кровли</h3>
            <form action="" class="calculator__form">
                <div class="calculator__label">Материалы кровельного настила</div>
                <select class="calculator__input form-select" aria-label="Default select example">
                    <option style="font-size: 10px" selected="">профнастил</option>
                    <option style="font-size: 10px" value="1">Металлочерепица</option>
                    <option style="font-size: 10px" value="2">Ондулин</option>
                    <option style="font-size: 10px" value="3">Шифер</option>
                </select>
                <div class="col-auto">
                    <label for="exampleFormControlInput1"
                           class="calculator__label form-label">Ширина</label>
                    <input type="number" class="calculator__input form-control"
                           id="exampleFormControlInput1"
                           placeholder="1 метр">
                </div>
                <div class="col-auto">
                    <div class="col-auto">
                        <label for="exampleFormControlInput2"
                               class="calculator__label form-label">Длина</label>
                        <input type="number" class="calculator__input form-control"
                               id="exampleFormControlInput2"
                               placeholder="2 метра">
                    </div>
                </div>
                <button type="submit" class="btn item-info__btn">Рассчитать</button>
                <div class="calculator__result"><span>всего:</span> 9999 грн</div>
            </form>
        </div>
    </div>
    <div class="calculator__image">
        <div class="calculator__image-container">
            <h2 class="calculator__image-title">калькулятор ремонта
            </h2>
            <div class="calculator__image-bg">
                <img src="/img/home/house-calc.png" alt="">
            </div>
        </div>
    </div>
</section>
