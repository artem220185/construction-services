
<section class="page-home__slider slider-home">
    <div class="slider-home__title-bg"></div>
    <div class="slider-home__container _container">
        <h2 class="slider-home__title">отзывы клинетов</h2>
        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
             class="swiper mySwiper">
            <div class="parallax-bg" data-swiper-parallax="-23%"></div>
            <div class="swiper-wrapper">
                @foreach($reviews as $review)
                    <div class="swiper-slide">
                        <div class="swiper-slide-container">
                            <div class="subtitle" data-swiper-parallax="-200">{{$review->author}}</div>
                            <div class="text" data-swiper-parallax="-100">
                                <p>{{$review->message}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<section><h3 class="review"><a href="{{ route('review_form') }}">Оставить отзыв</a></h3></section>
