<a id="about"></a>
<section class="home-page__about about-home">
    <div class="about-home__container ">
        <h2  class="about-home__title">Почему мы?</h2>
        <div class="about-home__row">
            <div class="about-home__column">
                <div class="about-home__item ">
                    <div class="about-home__item-container">
                        <div class="about-home__item-icon item-icon-1"></div>
                        <div class="about-home__item-title item-title-dark">профессионально</div>
                        <div class="about-home__item-subtitle item-subtitle-dark">Наши мастера обаладают
                            большим
                            опытом
                            и
                            выполняют все этапы работ в строгом соотвествии с тенологией
                        </div>
                    </div>
                </div>
                <div class="about-home__item  bg-item-dark">
                    <div class="about-home__item-container">
                        <div class="about-home__item-icon item-icon-2"></div>
                        <div class="about-home__item-title bg-item-dark">Отзывчивость</div>
                        <div class="about-home__item-subtitle">Принимаем во внимание все предпочтения
                            клиента
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-home__column">
                <div class="about-home__item  bg-item-dark">
                    <div class="about-home__item-container">
                        <div class="about-home__item-icon item-icon-3"></div>
                        <div class="about-home__item-title">Использование качественных материалов</div>
                        <div class="about-home__item-subtitle">Мы пользуемся услугами проверенных
                            поставщиков
                        </div>
                    </div>
                </div>
                <div class="about-home__item ">
                    <div class="about-home__item-container">
                        <div class="about-home__item-icon item-icon-4"></div>
                        <div class="about-home__item-title item-title-dark">Жесткая привязка к срокам</div>
                        <div class="about-home__item-subtitle item-subtitle-dark">Мы выполняем все ремонтные
                            работы
                            в
                            строго оговоренное время
                        </div>
                    </div>
                    <a id="review"></a>
                </div>
            </div>
        </div>
    </div>
</section>

