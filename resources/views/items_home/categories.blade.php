<a id="categorys"></a>
@foreach($categories as $category)
    @if(( $category->id % 2 ) !== 0 )
        @if(( ($category->id-1) % 2 ) !== 0 )
            <section class="home-page__roofing services">
                <div class="services__container _container">
                    <div class="services__block">
                        <div class="services__item-left item-left">
                            <h2 class="right-item__title right-item__title-mob">{{ $category->title }}
                                <br> работы</h2>
                            <div class="item-left__img">
                                <img src="/img/home/03.jpg" alt="Дом">
                            </div>
                            <div class="item-left__info item-info">
                                <div class="item-info__text">{{ $category->text }} </div>
                            </div>
                            <a class="item-info__btn btn"
                               href="{{ route('category',['slug'=>$category->slug]) }}">
                                подробнее</a>
                        </div>

                        <div class="services__item-right right-item right-item-hidden">
                            <h2 class="right-item__title services-right-item__title">{{ $category->title }} <br>
                                работы
                            </h2>

                            <div class="item-right__img">
                                <img src="{{ $category->main_image_url}}" alt="Дом">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <section class="home-page__facade services-small">
                <div class="services__container _container">
                    <h2 class="services__title">{{ $category->title }} работы</h2>
                    <h2 class="services__title services__title-mob">
                        {{ $category->title }} <br> работы
                    </h2>
                    <div class="services__block">
                        <div class="services__item-left item-left">
                            <div class="item-left__img">
                                <img src="{{ $category->main_image_url}}" alt="Дом">
                            </div>
                        </div>

                        <div class="services__item-right right-item">
                            <div class="item-left__info item-info">
                                <div class="item-info__text">
                                    {{ $category->text }}
                                </div>
                            </div>
                            <a class="item-info__btn btn"
                               href="{{ route('category',['slug'=>$category->slug]) }}">
                                подробнее
                            </a>
                        </div>
                    </div>

                </div>
            </section>
        @endif

    @else
        @if(( ($category->id-1) % 2 ) === 0 )
            <section class="home-page__facade services-small">
                <div class="services__container _container">
                    <h2 class="services__title">{{ $category->title }} работы</h2>
                    <h2 class="services__title services__title-mob">
                        {{ $category->title }} <br> работы
                    </h2>
                    <div class="services__block">
                        <div class="services__item-left item-left">
                            <div class="item-left__img">
                                <img src="{{ $category->main_image_url}}" alt="Дом">
                            </div>
                        </div>

                        <div class="services__item-right right-item">
                            <div class="item-left__info item-info">
                                <div class="item-info__text">
                                    {{ $category->text }}
                                </div>
                            </div>
                            <a class="item-info__btn btn"
                               href="{{ route('category',['slug'=>$category->slug]) }}">
                                подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <section id="{{$category->id}}" class="home-page__roofing services">
                <div class="services__container _container">
                    <div class="services__block">
                        <div class="services__item-left item-left">
                            <h2 class="right-item__title right-item__title-mob">{{ $category->title }}
                                <br> работы</h2>
                            <div class="item-left__img">
                                <img src="{{ $category->main_image_url }}" alt="Дом">
                            </div>
                            <div class="item-left__info item-info">
                                <div class="item-info__text">{{ $category->text }} </div>
                            </div>
                            <a class="item-info__btn btn"
                               href="{{ route('category',['slug'=>$category->slug]) }}">
                                подробнее</a>
                        </div>

                        <div class="services__item-right right-item right-item-hidden">
                            <h2 class="right-item__title services-right-item__title">{{ $category->title }} <br>
                                работы
                            </h2>

                            <div class="item-right__img">
                                <img src="{{$category->second_image_url}}" alt="Дом">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endif
@endforeach

