@extends('layouts.home',['title'=>"Заказ"])

@section('content')
    <form action="{{ route('review.create') }}" method="post" class="form__order-service order-service">
        @csrf
        <div class="order-service__container _container">
            <div class="form">
                <div class="form__title">Оставить отзыв</div>
                <div class="form__item">
                    <label for="formName" class="form__label">Имя*</label>
                    <input id="formName" type="text" name="author" value="{{old('author')}}"
                           class="form__input _req form-control {{ $errors->has('author') ? 'is-invalid' : '' }}">
                    @if($errors->has('author'))
                        <div class="invalid-feedback">
                            <strong>Напишите имя</strong>
                        </div>
                    @endif

                </div>
                <div class="form__item">
                    <label for="formMessage" class="form__label">Сообщние</label>
                    <textarea value="{{old('message')}}" maxlength="500" name="message" id="formMessage"
                    class="form__input"></textarea>
                    @if($errors->has('message'))
                        <div class="invalid-feedback">
                            <strong>Напишите комментарий</strong>
                        </div>
                    @endif
                </div>
                <button type="submit" class="form__button btn">Отправить</button>
            </div>
        </div>
    </form>
@endsection
