
@extends('layouts.home',['title'=>"Заказ"])

    @section('content')
        <form action="{{ route('order.create') }}" method="post" class="form__order-service order-service">
            @csrf
            <div class="order-service__container _container">
                <div class="form">
                    <div class="form__title">Заказать звонок</div>
                    <div class="form__item">
                        <label for="formName" class="form__label">Имя*</label>
                        <input id="formName" value="{{old('name')}}" type="text" name="name" class="form__input _req
                    form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">

                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                <strong>Напишите имя </strong>
                            </div>
                        @endif
                    </div>

                    <div class="form__item">
                        <label for="formTel" class="form__label ">Номер телефона*</label>
                        <input id="formTel" type="tel" name="phone" class="form__input _req" pattern="^\+380\d{9}$"
                               minlength="13" maxlength="13" value="+380"/>
                    </div>

                    {{--        title            --}}
                    <div class="form__item">
                        <label for="formTel" class="form__label ">Сообщение</label>
                        <textarea type="text" class="form-control form__input _req"
                                  value="{{ old('message') }}" name="message" id="exampleInputTitle"
                                  placeholder="не обязательно"></textarea>
                        @error('message')
                        <br>
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                    <button type="submit" class="form__button btn">Отправить</button>
                </div>

            </div>
        </form>
    @endsection
