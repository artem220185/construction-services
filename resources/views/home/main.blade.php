@extends('layouts.home',['title'=>"Главная"])

@section('content')

    <section class="home-page__main main-home">

        <div class="main-home__container _container">
            <div class="main-home__block">
                <div class="main-home__item-left item-left">
                    <div class="item-left__bg">
                        <img src="/img/home/house-big.png" alt="Дом">
                    </div>
                </div>
                <div class="main-home__item-right right-item">
                    @if (session('status'))
                        <div class="alert alert-success d-flex" role="alert">
                            <div>
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif
                    <h1 class="right-item__title mb-95">
                        @isset($titlesSorted)
                            @foreach  ($titlesSorted as $key)
                                {{$key}}
                                <br>
                            @endforeach
                        @endisset
                        <span>работы</span>
                    </h1>
                    <h1 class="right-item__title title-mob">
                        @isset($titlesSorted)
                            @foreach  ($titlesSorted as $key)
                                {{$key}}
                            @endforeach
                            <br>
                        @endisset
                        <br><span>работы</span>
                    </h1>

                    <div class="right-item__btn popup-btn">
                        <a href="{{ route('order_form') }}" class="btn">Перезвонить мне</a>
                    </div>
                </div>
            </div>
        </div>

    </section>

    {{--categories--}}
    @include('items_home.categories')
    {{--    about--}}
    @include('items_home.about')
    {{--reviews--}}
    {{--    @include('items_home.reviews')--}}

        <section class="page-home__slider slider-home">
            <div class="slider-home__title-bg"></div>
            <div class="slider-home__container _container">
                <h2 class="slider-home__title">отзывы клиентов</h2>
                <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
                     class="swiper mySwiper">
                    <div class="parallax-bg" data-swiper-parallax="-23%"></div>
                    <div class="swiper-wrapper">
                        @foreach($reviews as $review)
                            <div class="swiper-slide">
                                <div class="swiper-slide-container">
                                    <div class="subtitle" data-swiper-parallax="-200">{{$review->author}}</div>
                                    <div class="text" data-swiper-parallax="-100">
                                        <p>{{$review->message}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </section>
    <section><h3 class="review"><a href="{{ route('review_form') }}">Оставить отзыв</a></h3></section>


    {{--        calculator--}}
    {{--    @include('items_home.calculator')--}}

@endsection()
