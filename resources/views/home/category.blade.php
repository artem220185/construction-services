@extends('layouts.home',['title'=>$category->title.'работы'])

@section('content')
    <section class="catalog-page__main catalog">
        <div class="catalog__container _container">
            <div class="catalog__title">{{ $category->title }} работы</div>
            <div class="catalog__row">
                @foreach($category->getMedia() as $media)
                    <div class="catalog__column">
                        <div class="catalog__item">
                            <img src="{{$media->getUrl()}}" alt="{{ $category->title }}">
                        </div>
                    </div>
                @endforeach
            </div>
            {{--  slider --}}
            <div class="catalog__slider slider-home">

                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        @foreach($category->getMedia() as $media)
                            <div class="swiper-slide">
                                <div class="swiper-slide-item">
                                    <img src="{{$media->getUrl()}}">
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <div class="catalog__text">
                <p>{{ $category->text }}</p>
            </div>
            <div class="catalog__btn">
                <a href="{{ route('order_form') }}" class="btn">
                    Заказать
                </a>
            </div>
        </div>
    </section>
@endsection

