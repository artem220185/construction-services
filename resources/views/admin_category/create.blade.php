@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Добавить новую категорию</h3>
                </div>
                <!-- form start -->
                <form enctype="multipart/form-data"
                      method="POST" action="{{ route('admin_category.store') }}">
                    @csrf
                    <div class="card-body">
                        {{--title--}}
                        <div class="form-group">
                            <label for="exampleInputTitle">Название категории</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                   value="{{ old('title') }}" name="title" id="exampleInputTitle"
                                   placeholder="название работы">
                            @error('title')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{--text--}}
                        <div class="form-group">
                            <label for="exampleInputText">Описание работы</label>
                            <input type="text" class="form-control @error('text') is-invalid @enderror"
                                   value="{{ old('text') }}" name="text" id="exampleInputText"
                                   placeholder="описание работы">
                            @error('text')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- add img --}}
                        <div class="form-group">
                            <label>Добавить изображение</label>
                            <br>
                            <input type="file" name="img[]" multiple accept=".jpg,.png,.jpeg">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
