@extends('adminlte::page')

@section('content')
    <section class="content">
        <!-- Default box -->
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="row">
                        @foreach($category->getMedia() as $media)
                            <div class="product-image-thumb col-6 col-sm-3 mt-3 position-relative">
                                <img  src="{{$media->getUrl()}}" alt="{{ $category->title }}">
                                <form class="position-absolute top-0 end-0"
                                      data-placement="top" title="Удалить"
                                      action="{{ route('delete_img',$media->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-secondary"
                                            type="submit">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                             fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path
                                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                                            <path fill-rule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
                                        </svg>
                                    </button>
                                </form>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <h3 class="my-3">Изменить категорию "{{ $category->title }} работы"</h3>
                    <hr>
                    <!-- form  title and text-->
                    <form method="POST"
                          enctype="multipart/form-data"
                          action="{{ route('admin_category.update',["admin_category"=>$category->id]) }}">
                        @csrf
                        @method('PUT')
                        <div class="card-arrow">
                            {{--title--}}
                            <div class="form-group">
                                <label for="exampleInputTitle">Название категории</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror"
                                       value="{{ old('title') ?? $category->title }}"
                                       name="title" id="exampleInputTitle"
                                       placeholder="название работы">
                                @error('title')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            {{--text--}}
                            <div class="form-group">
                                <label for="exampleInputText">Описание работы</label>
                                <textarea type="text" name="text"
                                          class="form-control @error('text') is-invalid @enderror"
                                          value="{{ old('text')?? $category->text}}"
                                          id="exampleInputText"
                                          placeholder="описание работы">{{ old('text')?? $category->text}}</textarea>
                                @error('text')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group mt-4">
                                <label>Добавить изображение</label>
                                <div>
                                    <input type="file" name="img[]" multiple accept=".jpg,.png,.jpeg">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>

                    <hr>
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route('admin_category.index') }}">Назад</a>
                    </div>
                    {{---------------------}}
                    {{--                        <div class="btn btn-default btn-lg btn-flat">--}}
                    {{--                            <i class="fas fa-heart fa-lg mr-2"></i>--}}
                    {{--                            Add to Wishlist--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{---------------------}}

                </div>
            </div>
            <div class="row mt-4">
            </div>
        </div>
        <!-- /.card-body -->
    </section>
@endsection
