@extends('adminlte::page')

@section('content')

    <div class="row">
        @if (session('status'))
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path
                        d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </symbol>
            </svg>
            <div class="alert alert-success d-flex align-items-center ml-2" role="alert">
                <svg class="bi flex-shrink-0 mr-3 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use
                        xlink:href="#check-circle-fill"/>
                </svg>
                <div>
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Список категорий работ</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table-striped table table-bordered table-hover dataTable"
                                       role="grid"
                                       aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">
                                        <th>Id</th>
                                        <th>Название работы (категория)</th>
                                        <th>Описание</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr role="row">
                                            <td>{{$category->id}}</td>
                                            <td>{{$category->title}}</td>
                                            <td>{{$category->text}}</td>
                                            <td>
                                                <a type="button" class="btn btn-block btn-outline-info"
                                                   href="{{ route('admin_category.edit',
                                                   ['admin_category'=>$category->id]) }}">изменить
                                                </a>
                                                <form class="btn-block"
                                                      action="{{ route('admin_category.destroy',
                                                      ['admin_category'=>$category->id] )}}"
                                                      method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input class="btn btn-block btn-outline-danger" name="delete"
                                                           type="submit"
                                                           value="удалить">
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">Id</th>
                                        <th rowspan="1" colspan="1">Название работы (категория)</th>
                                        <th rowspan="1" colspan="1">Описание</th>
                                        <th rowspan="1" colspan="1">Действие</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('js')--}}
{{--    <script>--}}
{{--        $(function () {--}}
{{--            $('#example2').DataTable({--}}
{{--                "paging": true,--}}
{{--                "lengthChange": false,--}}
{{--                "searching": true,--}}
{{--                "ordering": true,--}}
{{--                "info": true,--}}
{{--                "autoWidth": false,--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
