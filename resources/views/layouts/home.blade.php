<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,regular,500,600,700,800,900"
          rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">

    <title>{{$title ?? ''}}</title>
</head>

<body>
<div class="wrapper">
    <header class="header">
        <div class="header__container">
            <a href="{{route('main')}}" class="header__logo"></a>
            <div class="header__menu menu">
                <div class="menu__icon">
                    <span></span>
                </div>
                <nav class="menu__body">
                    <ul class="menu__list">
                        <li>
                            <a href="/#categorys" class="menu__link">услуги</a>
                        </li>
                        <li>
                            <a href="/#about" class="menu__link">почему мы?</a>
                        </li>
                        <li>
                            <a href="/#review" class="menu__link">отзывы</a>
                        </li>
                        {{--                        ссылка на калькулятор--}}
                        {{--                        <li><a href="" class="menu__link">калькулятор</a></li>--}}
                        <li>
                            <a href="/#call" class="menu__link">контакты</a>
                        </li>

                        @if (Route::has('login'))
                            @auth
                                <li>
                                    <a href="{{ route('admin_category.index') }}"
                                       class=" menu__link text-sm text-gray-700underline ">Админка</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                        <input value="Выйти" type="submit">
                                    </form>
                                </li>
                            @else
                                <li class="link-icon">
                                    <a title="Вход в админку" href="{{ route('login') }}" class="menu__link">
                                        <img src="/img/icons/user.svg" alt="">
                                    </a>
                                </li>
                            @endauth
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="footer__container _container">
            {{--<div class="footer__title">Контакты</div>--}}
            <div class="footer__row">
                <div class="footer__img">
                    <a href=""></a>
                </div>
                <div class="footer__column">
                    <nav class="menu__body">
                        <ul class="menu__list">
                            <li>
                                <a href="" class="menu__link">услуги</a>
                            </li>
                            <li>
                                <a href="#about" class="menu__link">почему мы?</a>
                            </li>
                            <li>
                                <a href="#review" class="menu__link">отзывы</a>
                            </li>
                            {{-- ссылка на калькулятор--}}
                            {{--<li><a href="" class="menu__link">калькулятор</a></li>--}}
                            <li>
                                <a href="/#call" class="menu__link">контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div  class="footer__call call">
                    <div id="call" class="call__info"><a href="tel:+380969999999">+38 (096) 999-99-99</a></div>
                    <div class="call__btn">
                        <a href="{{ route('order_form') }}">Перезвонить мне</a>
                    </div>
                </div>
            </div>
            <div class="footer__copy">© 2021 Все права защищены.</div>
        </div>
    </footer>
</div>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="/js/app.js"></script>
</body>

</html>

