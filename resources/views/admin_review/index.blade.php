@extends('adminlte::page')

@section('content')

    <div class="row">
        @if (session('status'))
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                    <path
                        d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </symbol>
            </svg>
            <div class="alert alert-success d-flex align-items-center ml-2" role="alert">
                <svg class="bi flex-shrink-0 mr-3 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use
                        xlink:href="#check-circle-fill"/>
                </svg>
                <div>
                    {{ session('status') }}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Список комментариев</h3>
                </div>
                <div class="card-body">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table-striped table table-bordered table-hover dataTable"
                                       role="grid"
                                       aria-describedby="example2_info">
                                    <thead>
                                    <tr role="row">
                                        <th>Id</th>
                                        <th>Автор</th>
                                        <th>Комментарий</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reviews as $review)
                                        <tr role="row">
                                            <td>{{$review->id}}</td>
                                            <td>{{$review->author}}</td>
                                            <td>{{$review->message}}</td>
                                            <td>
                                                <form method="get"
                                                      action="{{ route('admin_review.show',
                                                      ['admin_review'=>$review->id]) }}"
                                                      role="form"
                                                class="d-flex p-2 bd-highlight">
                                                    @csrf
                                                    @if( $review->status == "Активный")
                                                        <input class="mt-2 btn btn-block btn-outline-success btn-sm"
                                                               type="submit"
                                                               value="{{ $review->status }}">
                                                    @else
                                                        <input class="mt-2 btn btn-block btn-outline-secondary btn-sm"
                                                               type="submit"
                                                               value="{{ $review->status }}">
                                                    @endif
                                                </form>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin_review.edit',
                                                      ['admin_review'=>$review->id] )}}"
                                                   type="button"
                                                   class="btn btn-block btn-outline-info btn-sm">Изменить</a>
                                                <form action="{{ route('admin_review.destroy',
                                                      ['admin_review'=>$review->id] )}}"
                                                      method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input class="mt-2 btn btn-block btn-outline-danger btn-sm" name="delete"
                                                           type="submit"
                                                           value="Удалить">
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">Id</th>
                                        <th rowspan="1" colspan="1">Автор</th>
                                        <th rowspan="1" colspan="1">Комментарий</th>
                                        <th rowspan="1" colspan="1">Статус</th>
                                        <th rowspan="1" colspan="1">Удалить</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                {{ $reviews->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@section('js')--}}
{{--    <script>--}}
{{--        $(function () {--}}
{{--            $('#example2').DataTable({--}}
{{--                "paging": true,--}}
{{--                "lengthChange": false,--}}
{{--                "searching": true,--}}
{{--                "ordering": true,--}}
{{--                "info": true,--}}
{{--                "autoWidth": false,--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
