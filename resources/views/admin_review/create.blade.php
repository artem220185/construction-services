<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,regular,500,600,700,800,900"
          rel="stylesheet"/>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <title>Отставить отзыв</title>
</head>

<body>
<div class="wrapper">
    <form action="{{ route('admin_review.store') }}" method="post" class="form__order-service order-service">
        @csrf
        <div class="order-service__container _container">
            <div class="form">
                <div class="form__title">Оставить отзыв</div>
                <div class="form__item">
                    <label for="formName" class="form__label">Имя*</label>
                    <input id="formName" type="text" name="author" value="{{old('author')}}"
                           class="form__input _req form-control {{ $errors->has('author') ? 'is-invalid' : '' }}">
                    @if($errors->has('author'))
                        <div class="invalid-feedback">
                            <strong>Напишите имя</strong>
                        </div>
                    @endif

                </div>
                <div class="form__item">
                    <label for="formMessage" class="form__label">Сообщние</label>
                    <textarea value="{{old('message')}}" maxlength="500" name="message" id="formMessage"
                    class="form__input"></textarea>
                    @if($errors->has('message'))
                        <div class="invalid-feedback">
                            <strong>Напишите комментарий</strong>
                        </div>
                    @endif
                </div>
                <button type="submit" class="form__button btn">Отправить</button>
            </div>
        </div>
    </form>
</div>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
</body>

</html>
