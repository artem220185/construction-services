@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Изменить отзыв</h3>
                </div>
                <form method="POST" action="{{ route('admin_review.update',["admin_review"=>$review->id]) }}">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        {{--author--}}
                        <div class="form-group">
                            <label for="exampleInputTtitle">Название категории</label>
                            <input type="text" class="form-control @error('author') is-invalid @enderror"
                                   value="{{ old('title') ?? $review->author }}"
                                   name="author" id="exampleInputTitle"
                                   placeholder="название работы">
                            @error('author')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{--text--}}
                        <div class="form-group">
                            <label for="exampleInputText">Описание работы</label>
                            <textarea type="text" class="form-control @error('message') is-invalid @enderror"
                                    name="message" id="exampleInputText"
                                   placeholder="описание работы">value="{{ old('text')?? $review->message}}"</textarea>
                            @error('message')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
