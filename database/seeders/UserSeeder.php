<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Roma';
        $user->password = Hash::make('0000');
        $user->email = 'Admin@admin.com';
        $user->phone = '+380971234567';
        $user->role = 'admin';
        $user->remember_token = Str::random(10);
        $user->save();
    }
}
