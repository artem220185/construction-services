<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Category
 * @property-read $main_image_url
 * @package App\Models
 */
class Category extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, HasSlug;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'text','slug'];

    public function getMainImageUrlAttribute()
    {
        $media_url = $this->getFirstMediaUrl();
        return $media_url ?: '/images/no-image.jpg';
    }

    public function getSecondImageUrlAttribute()
    {
        $items = $this->getMedia();
        if ($items->count() > 1) {
            return $items[1]->getUrl();
        } else {
            return '/images/no-image.jpg';
        }
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(15);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
