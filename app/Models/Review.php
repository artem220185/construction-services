<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['author', 'message'];

    public static function updateData($id, array $review)
    {
        return Review::where('id', $id)
            ->update($review);
    }

    public static function changeStatus($id)
    {
        $status = Review::find($id);
        if ($status->status === 'Не активный') {
            $status->status = 'Активный';
            $status->save();
        } else {
            $status->status = 'Не активный';
            $status->save();
        }
    }

}
