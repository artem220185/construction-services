<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'max:120',
                'string',
                Rule::unique('categories')->ignore($this->route('admin_category'))
            ],
            'text' => [
                'required',
                'max:1000',
                'string',
                Rule::unique('categories')->ignore($this->route('admin_category'))
            ],
            'img'=> ['array', 'nullable'],
            'img.*' => [
                'image',
                'mimes:jpg,bmp,png,jpeg',
                'max:20480'
            ]
        ];
    }
}
