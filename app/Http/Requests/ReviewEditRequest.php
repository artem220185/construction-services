<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ReviewEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author' => [
                'required',
                'min:3',
                'max:50',
                Rule::unique('reviews')->ignore($this->route('admin_review'))
            ],
            'message' => [
                'string',
                'min:3',
                'max:500',
                Rule::unique('reviews')->ignore($this->route('admin_review'))
            ]
        ];
    }
}
