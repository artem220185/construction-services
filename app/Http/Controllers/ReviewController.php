<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Http\Requests\ReviewRequest;

class ReviewController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ReviewRequest $request)
    {
        if (!empty($request)) {
            Review::create([
                'author' => $request->author,
                'message' => $request->message,
            ]);
            return redirect(route('main'));
        }
        return redirect(route('form_review'));
    }
}
