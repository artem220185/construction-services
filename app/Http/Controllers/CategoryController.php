<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Review;

class CategoryController extends Controller
{
    public function index()
    {
        $titlesSorted = [];
        foreach (Category::all() as $item) {
            if (iconv_strlen($item->title) < 15) {
                $titlesSorted[] = $item->title;
            }
        }

        return view('home.main',
            ['titlesSorted' => $titlesSorted,
                'categories' => Category::all(),
                'reviews' => Review::where('status', 'Активный')->get()
            ]);
    }

    public function showCategory($slug)
    {
        return view('home.category', [
            'category' => Category::whereSlug($slug)->firstOrFail()
        ]);
    }
}
