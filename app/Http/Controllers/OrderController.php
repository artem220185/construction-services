<?php

namespace App\Http\Controllers;

use App\Mail\NewOrder;
use App\Models\Order;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{

    public function create(OrderRequest $request)
    {
        if (!empty($request)) {
            $order = new Order();
            $order->fill($request->only(['name', 'phone', 'message']));
            $order->save();

            Mail::send(new NewOrder($order));

            return redirect(route('main'))
                ->with('status', 'Заказ оформлен. Мы перезвоним в ближайшее время');
        }
        return redirect(route('order.create'));
    }
}
