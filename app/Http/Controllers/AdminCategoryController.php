<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryAddRequest;
use App\Http\Requests\CategoryEditRequest;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class AdminCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_category.index', [
            'categories' => Category::paginate(6)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CategoryAddRequest $request
     * @return \App\Http\Requests\CategoryAddRequest
     */
    public function store(CategoryAddRequest $request)
    {
        $category = new Category();
        $category->fill($request->only(['title', 'text']));
        $category->save();

        if ($request->hasFile('img')) {
            foreach ($request->file('img') as $imgFile) {
                $category->addMedia($imgFile)->toMediaCollection();
            }
        }
        return redirect(route('admin_category.index'))->with('status', 'Категория "' . $request->title . '" добавлена');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin_category.edit', [
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\CategoryEditRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryEditRequest $request, $id)
    {
        /**
         * @var Category $category
         */
        $category = Category::find($id);
        $category->fill($request->only(['title', 'text']));
        $category->save();

        if ($request->hasFile('img')) {
            foreach ($request->file('img') as $imgFile) {
                $category->addMedia($imgFile)->toMediaCollection();
            }
        }

        return redirect()
            ->route('admin_category.edit', $id)
            ->with('status', 'Категория "' . $request->title . '" изменина'
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return back()
            ->with('status', 'Категория "' . $id . '" удалена');
    }

    public function deleteMedia($id)
    {
        Media::destroy($id);
        return back()->with('status', 'Картинка удалена');
    }
}
