<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Http\Requests\ReviewRequest;
use App\Http\Requests\ReviewEditRequest;

class AdminReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_review.index', [
            'reviews' => Review::paginate(8)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_review.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewRequest $request)
    {
        $review = Review::create(
            $request->all()
        );
        return redirect(route('admin_review.index'))->with('status', 'Комментарий "' . $review->id . '" добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Review::changeStatus($id);
        return redirect(route('admin_review.index'))
            ->with('status', 'Комментарий '.$id.' "' . Review::find($id)->status .'"');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin_review.edit',
            ['review' => Review::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewEditRequest $request, $id)
    {
        Review::updateData($id, $request->except(['_token','_method']));
        return redirect()
            ->route('admin_review.index')
            ->with('status', 'Отзыв "' . $id . '" изменён');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::destroy($id);
        return redirect(route('admin_review.index'))
            ->with('status', 'Комментарий "'.$id.'" удалён');
    }
}
