<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Http\Requests\OrderRequestst;

class AdminOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin_order.index', [
            'orders' => Order::paginate(8)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);
        return redirect(route('admin_order.index'))
            ->with('status', 'Заказ удалён');
    }
}
